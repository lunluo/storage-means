import Store from './lib/store';
import { optionType, unifyOption } from './lib/type';
export default function cache(option?: optionType | unifyOption): Store | undefined;
