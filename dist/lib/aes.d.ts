import CryptoJS from 'crypto-js';
/**
 * AES 加密
 * @param word: 需要加密的文本
 * KEY: // 需要前后端保持一致
 * mode: ECB // 需要前后端保持一致
 * pad: Pkcs7 //前端 Pkcs7 对应 后端 Pkcs5
 */
export declare class AES {
    KEY: CryptoJS.lib.WordArray;
    constructor(secretKey: string);
    encrypt: (plaintext: string) => string;
    /**
     * AES 解密
     * @param jsonStr
     */
    decrypt: (jsonStr: string) => string;
}
