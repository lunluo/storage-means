export type optionType = 'local' | 'session' | 'memory';
export interface option {
    failureTime: number;
    encryption: boolean;
}
export interface unifyOption extends option {
    type?: optionType;
    namespace: string;
    secretKey: string;
}
export type filesItem = {
    key: string;
    value: string | unknown;
    type: string;
    watchs: Function[];
    option: option;
    timestamp?: number;
};
export type files = {
    [key: string]: filesItem;
};
export type watchOption = {
    type: 'get' | 'set';
    value: unknown;
};
