declare class Base64 {
    private _keyStr;
    encode(input: string): string;
    decode(input: string): string;
    _utf8_encode(string: string): string;
    _utf8_decode(utftext: string): string;
}
export default Base64;
