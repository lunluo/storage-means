import { option, unifyOption, files } from './type';
import { AES } from './aes';
export default class Store {
    store: Storage;
    files: files;
    unifyOption: unifyOption;
    aes: AES;
    constructor(store: Storage, unifyOption: unifyOption);
    set(key: string, value: unknown, tempOption?: option): void;
    get(key: string): string | {
        success: boolean;
        message: unknown;
        value: any;
    } | undefined;
    remove(key: string): void;
    clear(): void;
    on(key: string, callback: Function): void;
    off(key: string, callback: Function): void;
    private runWatch;
    private isExistInFiles;
    private setFiles;
    private getFiles;
    private keyFormat;
}
