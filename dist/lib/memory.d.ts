export default class Memory {
    mapValue: Map<any, any>;
    length: number;
    key: any;
    constructor();
    setItem(key: string, value: string | undefined): void;
    getItem(key: string): any;
    removeItem(key: string): void;
    clear(): void;
}
