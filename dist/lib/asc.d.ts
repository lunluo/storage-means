/**
 * @description: 加密
 * @param {*} word
 * @param {*} keyStr
 */
export declare function encrypt(word: any, keyStr?: any): string;
/**
 * @description: 解密
 * @param {*} word
 * @param {*} keyStr
 */
export declare function decrypt(word: any, keyStr?: any): string;
