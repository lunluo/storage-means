@[TOC](storage-means)

# 介绍
JavaScript缓存封装


# 基本使用
默认情况下会使用localstorage进行数据的存储
```javascript
import cache from 'storage-means'
const storage = new cache()
storage.set('number', 123)
const num = storage.get('number')
```
# 三种存储方式
## local
local是默认的数据存储方式，基于window.localStorage实现，会自动检测localStorage是否可以使用
```javascript
const storage = new cache('local')
storage.set('number', 123)
const num = storage.get('number')
```
## session
基于window.sessionStorage实现，会自动检测sessionStorage是否可以使用
```javascript
const storage = new cache('session')
storage.set('number', 123)
const num = storage.get('number')
```

## memory
将数据储存在类内部
```javascript
const storage = new cache('memory')
storage.set('number', 123)
const num = storage.get('number')
```
# 特殊配置
## 通用配置
```css
{
	failureTime: number, // 数据过期时间，过期则返回undefined
	encryption: boolean, // 数据是否加密
	type: optionType, // 使用什么存储方式
  	namespace: string, // 命名空间，在实际设置的key前自动拼接
  	secretKey: string, // 加密的秘钥
}
```

```javascript
const storage = new cache({ 
	failureTime: 2000, 
	encryption: true, 
	type: 'local', 
	namespace: 'test', 
	secretKey: '123' 
})
storage.set('number', 123)
const num = storage.get('number')
```
## 单项配置

```css
{
	failureTime: number, // 数据过期时间，过期则返回undefined
	encryption: boolean, // 数据是否加密
}
```

```javascript
storage.set('number', 111, { 
	failureTime: 30 * 1000, 
	encryption: true 
})
```
# 监听
## 开启监听

```javascript
function test () {
  console.log(...arguments)
}
storage.on('number', test)
```
## 关闭监听
```javascript
storage.off('number', test)
```
