import typescript from '@rollup/plugin-typescript'
import resolve from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'
import bable from '@rollup/plugin-babel'
import alias from '@rollup/plugin-alias'
import commonjs from '@rollup/plugin-commonjs'
export default {
  input: 'src/index.ts',
  output: [
    {
      file: 'dist/index.js',
      format: 'es',
    },
  ],
  plugins: [
    typescript({
      tsconfig: './tsconfig.json',
    }),
    alias({
      resolve: ['.js'],
    }),
    commonjs(),
    resolve(),
    bable({
      exclude: 'node_modules/**',
      babelHelpers: 'bundled',
      presets: ['@babel/preset-env'],
    }),
    terser(),
  ],
}
