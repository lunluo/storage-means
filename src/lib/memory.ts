// 定义一个与localStorage和sessionStorage相同的变量储存机制
export default class Memory {
  mapValue
  length = 0
  key: any
  constructor() {
    this.mapValue = new Map()
    Object.defineProperty(this, 'length', {
      get() {
        return this.mapValue.size
      },
      set(v) {},
    })
  }
  setItem(key: string, value: string | undefined) {
    this.mapValue.set(key, value)
  }
  getItem(key: string) {
    return this.mapValue.get(key)
  }
  removeItem(key: string) {
    this.mapValue.delete(key)
  }
  clear() {
    this.mapValue.clear()
  }
}
